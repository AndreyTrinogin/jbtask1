## Application deployment

Artifact and deployment scripts are available in /distribution.  

For start application you should have docker-compose on computer.  
Official site: [Docker](https://www.docker.com/)

For start application go to /distribution and enter the command:
```shell
docker-compose up
```

After this you can go to browser page and use the application

```shell
http://localhost:9000/jbtask1/home
```

Fot stop application return to cmd and press ctrl+c.

## Build artifact (jar file)

./mvnw clean package

Artifact available in /target.  
For deploy artifact you should copy artifact from /target to /distribution.