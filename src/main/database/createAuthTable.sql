CREATE TABLE auth
(
    id         SERIAL,
    usr_name   VARCHAR(50),
    cookie     varchar(500),
    active     BOOLEAN,
    start_time TIMESTAMP,
    end_time   TIMESTAMP
);