package ru.trinogin.jbtask1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbtask1Application {

    public static void main(String[] args) {
        SpringApplication.run(Jbtask1Application.class, args);
    }

}
