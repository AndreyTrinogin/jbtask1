package ru.trinogin.jbtask1.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.trinogin.jbtask1.models.Authorization;
import ru.trinogin.jbtask1.services.AuthService;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Andrey Trinogin
 */

@Controller
public class MainController {

    private final AuthService authService;
    private static final Authorization fakeAuthorization = new Authorization();
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    public MainController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping("/home")
    public String home(Model model,
                       HttpServletRequest request) {

        Authorization authorization = authService.getAuthorization(request.getCookies());
        if (authorization != null) {
            logger.info("/home, User authorized, authorized userName: " + authorization.getUsrName());
        } else {
            authorization = fakeAuthorization;
        }
        model.addAttribute(authorization);
        return "home";
    }

    @GetMapping("/signin")
    public String enter(Model model,
                        HttpServletRequest request) {

        Authorization authorization = authService.getAuthorization(request.getCookies());
        if (authorization != null) {
            logger.info("/signin, User authorized, authorized userName: " + authorization.getUsrName());
        } else {
            authorization = fakeAuthorization;
        }
        model.addAttribute(authorization);
        return "signin";
    }

    @GetMapping("/signout")
    public String exit(Model model,
                       HttpServletRequest request) {

        authService.delAuthorization(request.getCookies());

        model.addAttribute("authorization", fakeAuthorization);
        return "redirect:/home";
    }

}
