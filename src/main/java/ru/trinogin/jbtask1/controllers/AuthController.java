package ru.trinogin.jbtask1.controllers;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ru.trinogin.jbtask1.services.AuthService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Andrey Trinogin
 */

@Controller
public class AuthController {

    private static final String googleApiURL = "googleApiURL";
    private static final String googleUserInfoURL = "googleUserInfoURL";
    private static final String redirectURI = "redirect_uri";
    private static final String responseType = "response_type";
    private static final String clientId = "client_id";
    private static final String clientSecret = "clientSecret";
    private static final String scopes = "scope";
    private static final String accessToken = "access_token";
    private static final String eq = "=";
    private static final String localhost = "http://localhost";
    private static final String googleAuthRespControllerAddr = "/signin/byGoogleCredResp";

    private static final Map<String, String> settingsMap = Stream.of(new String[][]{
            {googleApiURL, "https://accounts.google.com/o/oauth2/"},
            {googleUserInfoURL, "https://www.googleapis.com/oauth2/v1/userinfo"},
            {redirectURI, ""}, // filling in constr
            {responseType, "code"},
            {clientId, "47537324902-l3smaaagetp6fr2eh0vo2cdta460kn84.apps.googleusercontent.com"},
            {clientSecret, "Gr_Yh_oDtxtD6RNkgOiDs9zJ"},
            {scopes, "https://www.googleapis.com/auth/userinfo.profile"}
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private static final JSONParser jsonParser = new JSONParser();

    private final AuthService authService;


    @Autowired
    AuthController(Environment env, AuthService authService) {
        settingsMap.put(redirectURI, localhost + ":" + env.getProperty("server.port") +
                env.getProperty("server.servlet.context-path") + googleAuthRespControllerAddr);
        this.authService = authService;
    }

    /**
     * Redirect user to google-authorization page
     *
     * @return Google Auth page
     */
    @GetMapping("/signin/byGoogleCred")
    public String processGoogleAuthReq() {

        String googleAuthRequest = settingsMap.get(googleApiURL) + "auth?" + String.join(
                "&",
                String.join(eq, redirectURI, settingsMap.get(redirectURI)),
                String.join(eq, responseType, settingsMap.get(responseType)),
                String.join(eq, clientId, settingsMap.get(clientId)),
                String.join(eq, scopes, settingsMap.get(scopes))
        );
        return "redirect:" + googleAuthRequest;
    }

    /**
     * Process result of user google-authorization
     * Google auth redirects user to this page, after processing it's
     * set response cookie and redirect user to /home
     * From this page collects google token and google user data
     *
     * @param request Request for processing (received from google auth page)
     * @return /home Page with/without user data
     */
    @GetMapping(googleAuthRespControllerAddr)
    public String processGoogleAuthResp(HttpServletRequest request,
                                        HttpServletResponse response) {

        String authorizationCode = request.getParameter("code");
        logger.trace("Parsed, authcode: " + authorizationCode);

        if (authorizationCode == null || authorizationCode.isEmpty()) {
            logger.info("AuthCode is invalid, authorization by google failed, non-authorized redirect");
            return "redirect:/home";
        }

        String userName = getUserNameByGoogle(authorizationCode);
        if (userName == null) {
            logger.info("Authorization by google failed, non-authorized redirect");
            return "redirect:/home";
        }

        Cookie responseCookie = authService.createAuthorization(
                userName,
                request.getHeader("User-Agent"),
                request.getRemoteHost(),
                String.valueOf(request.getRemotePort()));

        responseCookie.setPath(request.getContextPath());
        response.addCookie(responseCookie);
        logger.trace("Set cookie to response");

        return "redirect:/home";
    }

    /**
     * The function gets the name of the authorized user from the Google api.
     * Several steps.
     * 1. With authorization code get access_token from google.
     * 2. Then with the authorization_code + access_token function get google.userprofile.name.
     *
     * @param authorizationCode - code for receiving access_token
     * @return name of the authorized user
     */
    private String getUserNameByGoogle(String authorizationCode) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        List<BasicNameValuePair> params = Arrays.asList(
                new BasicNameValuePair(clientId, settingsMap.get(clientId)),
                new BasicNameValuePair(clientSecret, settingsMap.get(clientSecret)),
                new BasicNameValuePair(redirectURI, settingsMap.get(redirectURI)),
                new BasicNameValuePair("grant_type", "authorization_code"),
                new BasicNameValuePair("code", authorizationCode));
        logger.trace("Params: " + params);

        String accessTokenValue = getAccessToken(httpClient, params);
        if (accessTokenValue == null) {
            logger.info("Access token is null, auth processing failed");
            return null;
        }

        String userData = getUserData(httpClient, authorizationCode, accessTokenValue);
        if (userData == null) {
            logger.info("User Data is null, auth processing failed");
            return null;
        }

        try {
            httpClient.close();
        } catch (IOException e) {
            logger.info("Failed to release system resources:");
            e.printStackTrace();
        }

        JSONObject jsonObject;
        try {
            jsonObject = (JSONObject) jsonParser.parse(userData);
        } catch (ParseException e) {
            logger.info("Failed to parse received json with user data");
            e.printStackTrace();
            return null;
        }

        return jsonObject.get("name").toString();
    }

    /**
     * The function get access_token from google.
     * List of params must contain authorization code.
     *
     * @param httpClient client for execute http requests.
     * @param params     List with params for request.
     * @return access_token.
     */
    private String getAccessToken(CloseableHttpClient httpClient,
                                  List<BasicNameValuePair> params) {
        HttpPost httpPostToken = new HttpPost(settingsMap.get(googleApiURL) + "token");
        logger.trace("Prepared URI for token request: " + httpPostToken.getURI());
        httpPostToken.setEntity(new UrlEncodedFormEntity(params, StandardCharsets.UTF_8));
        String accessTokenValue = null;

        try {
            logger.trace("Send access_token request");
            HttpResponse httpRespToken = httpClient.execute(httpPostToken);
            if (httpRespToken.getEntity() != null) {
                String content = EntityUtils.toString(httpRespToken.getEntity());
                logger.trace("Received response:\n" + content);

                JSONObject jsonObject = (JSONObject) jsonParser.parse(content);
                accessTokenValue = jsonObject.get("access_token").toString();
                logger.trace("Parsed access_token: " + accessTokenValue);
            } else {
                logger.info("Response is empty");
                return null;
            }

        } catch (IOException e) {
            logger.info("Error with execute http request:\n" + e);
            e.printStackTrace();
        } catch (ParseException parseException) {
            logger.info("Error with parse access_token :\n" + parseException);
            parseException.printStackTrace();
        }

        return accessTokenValue;
    }

    /**
     * The function get userData from google.
     *
     * @param httpClient       client for execute http requests.
     * @param authCode         authorization code
     * @param accessTokenValue access_token
     * @return String contains userData from google.
     */
    private String getUserData(CloseableHttpClient httpClient,
                               String authCode, String accessTokenValue) {
        String userData = null;
        HttpGet httpGetData = new HttpGet(settingsMap.get(googleUserInfoURL) + "?" +
                String.join("&",
                        String.join(eq, clientId, settingsMap.get(clientId)),
                        String.join(eq, clientSecret, settingsMap.get(clientSecret)),
                        String.join(eq, redirectURI, settingsMap.get(redirectURI)),
                        String.join(eq, "grant_type", "authorization_code"),
                        String.join(eq, "code", authCode),
                        String.join(eq, accessToken, accessTokenValue)
                ));
        logger.trace("Prepare URI for user data request: " + httpGetData.getURI());

        try {
            logger.trace("Send user data request");
            HttpResponse httpRespData = httpClient.execute(httpGetData);

            if (httpRespData.getEntity() != null) {
                userData = EntityUtils.toString(httpRespData.getEntity());
                logger.trace("Received user data:\n" + userData);
            } else {
                logger.info("User data is empty");
            }
        } catch (IOException e) {
            logger.info("Error with execute http request:\n" + e);
            e.printStackTrace();
        }

        return userData;
    }
}
