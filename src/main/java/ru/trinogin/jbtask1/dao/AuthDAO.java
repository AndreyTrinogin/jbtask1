package ru.trinogin.jbtask1.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.trinogin.jbtask1.models.Authorization;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author Andrey Trinogin
 */

@Component
public class AuthDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AuthDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Authorization auth) {
        jdbcTemplate.update("INSERT INTO auth (usr_name, cookie, active, start_time) VALUES (?, ?, ?, ?)",
                auth.getUsrName(), auth.getCookie(), auth.getActive(), Timestamp.from(Instant.now()));
    }

    public Authorization getActual(String encodedCookieValue) {
        return jdbcTemplate.query("SELECT * FROM auth WHERE cookie = ? AND active IS true",
                new BeanPropertyRowMapper<>(Authorization.class),
                encodedCookieValue).
                stream().findAny().orElse(null);
    }

    public void deactivate(String encodedCookieValue) {
        jdbcTemplate.update("UPDATE auth SET active = FALSE, end_time = ? WHERE cookie = ?",
                Timestamp.from(Instant.now()), encodedCookieValue);
    }
}
