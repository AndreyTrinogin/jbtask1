package ru.trinogin.jbtask1.models;

import java.sql.Timestamp;

/**
 * @author Andrey Trinogin
 */
public class Authorization {

    private int id;
    private String usrName;
    private String cookie;
    private boolean active;
    private Timestamp startTime;
    private Timestamp endTime;

    public Authorization() {
    }

    public Authorization(String usr_name, String cookieValue, boolean active) {
        this.usrName = usr_name;
        this.cookie = cookieValue;
        this.active = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsrName() {
        return usrName;
    }

    public void setUsrName(String usrName) {
        this.usrName = usrName;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }
}
