package ru.trinogin.jbtask1.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import ru.trinogin.jbtask1.dao.AuthDAO;
import ru.trinogin.jbtask1.models.Authorization;

import javax.servlet.http.Cookie;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @author Andrey Trinogin
 */

@Service
public class AuthService {

    private static final String cookieName = "Cookie.2021.jb";
    private static final String salt = "gAwwDACUhjs4yO0";

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);
    private final AuthDAO authDao;

    @Autowired
    public AuthService(AuthDAO authDAO) {
        this.authDao = authDAO;
    }

    /**
     * Evaluate value for cookie
     *
     * @param userName  userName from google
     * @param userAgent userAgent from request
     * @param userHost  userHost from request
     * @param userPort  userPort from request
     * @return value for cookie
     */
    private String encodeCookie(String userName, String userAgent, String userHost, String userPort) {
        return DigestUtils.md5DigestAsHex(
                String.join("_", userAgent, userHost, salt, userPort, userName).getBytes(StandardCharsets.UTF_8));
    }

    /**
     * Search cookie in received array.
     *
     * @param cookies array with cookie, from request
     * @return cookie object, if found; null if no our cookie in array
     */
    private Cookie getCookie(Cookie[] cookies) {
        if (cookies == null) {
            logger.info("Received cookies is empty");
            return null;
        }

        return Arrays.stream(cookies).
                filter(ck -> cookieName.equals(ck.getName())).
                findAny().
                orElse(null);
    }

    /**
     * @param cookies array with cookies,
     *                for search user in table with authorized users
     *                cookie from array is using as parameter for search
     * @return Authorization object, if user exists
     */
    public Authorization getAuthorization(Cookie[] cookies) {
        Cookie cookie = getCookie(cookies);
        if (cookie == null) {
            logger.info("Our cookie not found, non-authorized request");
            return null;
        }

        logger.trace("Found our cookie ");
        logger.trace(cookie.getName() + " : " + cookie.getValue());

        return authDao.getActual(cookie.getValue());
    }

    /**
     * Deactivate user authorization
     * @param cookies array with cookies,
     *                for search user in table with authorized users
     *                cookie from array is using as parameter for search
     */
    public void delAuthorization(Cookie[] cookies) {
        Cookie cookie = getCookie(cookies);
        if (cookie == null) {
            logger.info("Our cookie not found, non-authorized signout request");
            return;
        }

        logger.trace("Found our cookie ");
        logger.trace(cookie.getName() + " : " + cookie.getValue());
        logger.trace("Authorization deactivated");
        authDao.deactivate(cookie.getValue());
    }

    /**
     * Create authorization for new user
     * @param userName  userName from google
     * @param userAgent userAgent from request
     * @param userHost  userHost from request
     * @param userPort  userPort from request
     * @return Cookie object for setting in http - response
     */
    public Cookie createAuthorization(String userName, String userAgent, String userHost, String userPort) {
        logger.trace(String.join("\n", "Received params for create cookie: ", userName, userAgent, userHost, userPort));
        Cookie cookie = new Cookie(cookieName, encodeCookie(userName, userAgent, userHost, userPort));

        authDao.save(new Authorization(userName, cookie.getValue(), true));

        return cookie;
    }

}
